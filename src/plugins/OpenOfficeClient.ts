import _Vue from 'vue';
import { Store } from 'vuex';
import MQTT from 'async-mqtt';
import { AsyncMqttClient } from 'async-mqtt';

export interface OpenOfficeClientPluginContract {
  store: Store<any>;
  connection: {
    namespace?: string,
    host: string,
    port: string,
  };
}

export async function OpenOfficeClientPlugin(Vue: typeof _Vue, { store, connection }: OpenOfficeClientPluginContract) {
  // Register and Start MQTT Client
  const client = await MQTT.connect(`ws://${connection.host}:${connection.port}`);

  Vue.prototype.$mqttClient = client;

  client.on('message', (topic, payload) => {
    store.dispatch('devices/updateDevice', { topic, deviceManifest: JSON.parse(payload.toString()) });
  });
  client.on('error', (e) => {
    //
  });
  await client.subscribe(`${connection.namespace || 'turbine/toss'}/+/devices/+`);
  // TODO: Add new Store Module

  // Trigger store events on new devices
}

declare module 'vue/types/vue' {
  interface Vue {
    $mqttClient: AsyncMqttClient;
  }
}

