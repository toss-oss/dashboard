import { ActionContext } from 'vuex';
import LightBubble from '@/components/bubbles/LightBubble.vue';
import Vue from 'vue';
import TTSBubble from '@/components/bubbles/TTSBubble.vue';
import Mp3Bubble from '@/components/bubbles/Mp3Bubble.vue';
import SoundBoxBubble from '@/components/bubbles/SoundBoxBubble.vue';

interface DeviceState {
  devices: any;
}

interface UpdateDevicePayload {
  topic: string;
  deviceManifest: any;
}

const componentMapper: any = {
  light: LightBubble,
  tts_player: TTSBubble,
  mp3_player: Mp3Bubble,
  soundbox: SoundBoxBubble,
};

export default {
  namespaced: true,
  state: {
    devices: {},
  },
  mutations: {
    updateDevice(state: DeviceState, { topic, deviceManifest }: UpdateDevicePayload) {
      Vue.set(state.devices, topic, {
        topic,
        ...deviceManifest,
        component: componentMapper[deviceManifest.type],
      });
    },
  },
  actions: {
    async updateDevice({commit, state}: ActionContext<DeviceState, any>, {topic, deviceManifest}: UpdateDevicePayload) {
      if (!componentMapper.hasOwnProperty(deviceManifest.type)) {
        return;
      }

      commit('updateDevice', {topic, deviceManifest});
    },
  },
  getters: {
    devices: (state: DeviceState) => Object.values(state.devices),
  },
};
