import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import '@/assets/tailwind.scss';
import { OpenOfficeClientPlugin, OpenOfficeClientPluginContract } from './plugins/OpenOfficeClient';

Vue.config.productionTip = false;

const openOfficeClientPluginProperties: OpenOfficeClientPluginContract = {
  store,
  connection: {
    host: '10.20.80.103',
    port: '1884',
  },
};

Vue.use(OpenOfficeClientPlugin, openOfficeClientPluginProperties);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
