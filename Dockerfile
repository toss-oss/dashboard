# Multi-stage Build

## NodeJS to build Dist
FROM node:10.15 as builder
WORKDIR /usr/src/app

COPY package.json yarn.lock ./
RUN yarn install

COPY . .
RUN yarn run build

## Final Image
FROM nginx:1.15
COPY --from=builder /usr/src/app/dist /usr/share/nginx/html